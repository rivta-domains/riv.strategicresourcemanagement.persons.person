# README #

•	Instruktioner för hur man kommer igång med bitbucket http://rivta.se/bitbucket/git-sourcetree.html

### What is this repository for? ###

* Quick summary

Tjänstedomänen hanterar uppgifter om person som dels finns registrerade i folkbokföringsregistret hos Skatteverket, dels har tillkommit genom uttagande av reservidentiteter, dels har tillkommit genom personens egna kompletteringar kring kontaktuppgifter.

* Till [wiki](https://bitbucket.org/rivta-domains/best-practice/wiki)

* Till [ärendehantering](https://bitbucket.org/rivta-domains/riv.strategicresourcemanagement.persons.person/issues)